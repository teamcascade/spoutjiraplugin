package org.teamcascade.spoutjiraplugin.listeners;

import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.player.PlayerJoinEvent;
import org.spout.api.plugin.CommonPlugin;
import org.teamcascade.spoutjiraplugin.config.Configuration;
import org.teamcascade.spoutjiraplugin.helpers.task.Task;
import org.teamcascade.spoutjiraplugin.helpers.task.TaskExecutor;
import org.teamcascade.spoutjiraplugin.jira.client.ActivityStreamClient;
import org.teamcascade.spoutjiraplugin.jira.client.JiraUserClient;

import java.util.logging.Logger;

/**
 *
 */
public class LoginListener implements Listener
{
    private final Logger log;
    private final ActivityStreamClient activityStreamClient;
    private final JiraUserClient jiraUserClient;
    private final TaskExecutor taskExecutor;
    private final Configuration pluginConfig;

    LoginListener(CommonPlugin plugin, Configuration pluginConfig, JiraUserClient jiraUserClient, ActivityStreamClient activityStreamClient, TaskExecutor taskExecutor)
    {
        this.activityStreamClient = activityStreamClient;
        this.jiraUserClient = jiraUserClient;
        this.taskExecutor = taskExecutor;
        this.pluginConfig = pluginConfig;
        log = plugin.getLogger();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPlayerQuitEvent(final PlayerQuitEvent event)
    {
        taskExecutor.executeAsyncTask(new Task<Object>()
        {
            @Override
            public Object execute()
            {
                activityStreamClient.postActivity(event.getPlayer(), "<strong>" + event.getPlayer().getDisplayName() + "</strong> logged out of Minecraft", "<blockquote>" + event.getQuitMessage() + "</blockquote>");
                return null;
            }
        });
    }

    @EventHandler
    public void onPlayerJoinEvent(final PlayerJoinEvent event)
    {
        taskExecutor.executeAsyncTask(new Task<Object>()
        {
            @Override
            public Object execute()
            {
                if (pluginConfig.isDynamicUserCreationEnabled())
                {
                    if (!jiraUserClient.doesJiraUserExist(event.getPlayer().getName()))
                    {
                        jiraUserClient.createUser(event.getPlayer().getName());
                    }
                }

                activityStreamClient.postActivity(event.getPlayer(), "<strong>" + event.getPlayer().getDisplayName() + "</strong> logged in to Minecraft", "<blockquote>" + event.getJoinMessage() + "</blockquote>");
                return null;
            }
        });
    }


}

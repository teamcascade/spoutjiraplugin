package org.teamcascade.spoutjiraplugin.jira.client;

/**
 *
 */
public interface JiraClientFactory {

    public JiraClient getClient(String jiraBaseUrl, String locationCustomFieldId, String minecraftProjectKey, String adminUsername, String adminPassword);

}

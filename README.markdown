# SpoutJiraPlugin

![SpoutJiraPlugin][1]

What is SpoutJiraPlugin?
------------------------

**Minecraft** is an open-world sandbox game in which players mine, craft, explore and build an unlimited world made from blocks of different materials.

**JIRA** is a software application from [Atlassian](http://www.atlassian.com/) that lets developers, managers and other team members track their tasks and issues.

**Spout** and **Vanilla** is a Minecraft Server implementation which allows custom blocks, mobs and a lot more.


Using this plugin when playing Minecraft allows you to keep track of a 'to-do' list in Minecraft by creating and resolving JIRA issues.

This plugin was originally created by  [Joe Clark][11] for bukkit and you kind find the original code [here.][12]



Core Ideas:
-----------
- Allow creating of Jira Issues via Signs
- Allow closing of Jira Issues via destroying the Sign
- Allow listing of open Jira Issues via /jiraissues
- Allow teleporting to the location of the issue via /gotoissue

Who is TeamCascade?
-------------------

TeamCascade is the team behind the different projects originating from TeamCascade, we work together on our plugins, sometimes as a team, sometimes as one developer with some helping hands.

![Don Redhorse][20]   ![Duck][21]

 Don Redhorse     Dukmeister

Visit our [website][2].
Track and submit issues and bugs on our [issue tracker][3].

Getting Started
---------------

We will supply an updated documentation for Spout soon, but atm please go to the [orignal wiki][14] for information on how to set the plugin up.

Source
------
The latest and greatest source can be found on [BitBucket][4]
Download the latest builds from [Bamboo][5].
You can get maven artifacts from [Artifactory][6].
And you will find the documentation in our [Doxygen Repo][7]

License
-------
SpoutJiraPlugin is licensed under [TeamCascade Public License v1][8], but with a provision that files are released under the MIT license 180 days after they are published. Please see the `LICENSE.txt` file for details.
Also the original plugin was licensed by the included [LICENCE.txt][13]

Compiling
---------
SpoutJiraPlugin uses Maven to handle its dependencies.

* Install [Maven 2 or 3][9]
* Checkout this repo and run: `mvn clean package install`

Coding Standards
----------------------------------
* If / for / while / switch statement: if (conditions && stuff) {
* Method: public void method(Object paramObject) {
* No Spaces, Tab is preferred!
* No trailing whitespace
* Mid-statement newlines at a 200 column limit
* camelCase, no under_scores except constants
* Constants in full caps with underscores
* Keep the same formatting style everywhere
* You can use the TeamCascadeCodeScheme.xml to implement our coding formatting style in most IDE, [Intellij Idea][10]

Pull Request Standards
----------------------------------
* Sign-off on all commits!
* Finished product must compile successfully with `mvn`!
* No merges should be included in pull requests unless the pull request's purpose is a merge.
* Number of commits in a pull request should be kept to *one commit* and all additional commits must be *squashed*. Pull requests that make multiple changes should have one commit per change.
* Pull requests must include any applicable license headers. (These are generated when running `mvn clean`)

[1]: http://www.teamcascade.org/plugins/servlet/zenfoundation/zenservlet/designs/brands/teamcascade/images/logo1.png
[2]: http://www.teamcascade.org
[3]: http://issues.teamcascade.org
[4]: https://bitbucket.org/teamcascade/spoutjiraplugin
[5]: http://builds.teamcascade.org
[6]: http://artifacts.teamcascade.org
[7]: http://docs.teamcascade.org
[8]: http://www.teamcascade.org/display/AboutUs/TeamCascade+Public+License
[9]: http://maven.apache.org/download.html
[10]: http://musingsofaprogrammingaddict.blogspot.de/2010/03/import-code-style-settings-into.html
[11]: https://bitbucket.org/jaysee00
[12]: https://bitbucket.org/jaysee00/minecraftjiraplugin
[13]: https://bitbucket.org/teamcascade/spoutjiraplugin/src/886ffec31b640376c9b7aa6171af2300a1d3f4a3/LICENCE.txt?at=master
[14]: https://bitbucket.org/teamcascade/spoutjiraplugin/wiki/Home
[20]: http://www.gravatar.com/avatar/5715022800b638db4951afe80841b314.png?size=55
[21]: https://dl.dropbox.com/u/userid/file.png



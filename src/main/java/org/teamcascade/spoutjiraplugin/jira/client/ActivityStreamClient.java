package org.teamcascade.spoutjiraplugin.jira.client;

import org.spout.api.entity.Player;

/**
 *
 */
public interface ActivityStreamClient
{
    public void postActivity(Player actor, String titleHtml, String contentHtml);
}

package org.teamcascade.spoutjiraplugin.listeners;


import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.geo.World;
import org.spout.api.geo.cuboid.Block;
import org.spout.api.geo.discrete.Point;
import org.spout.api.material.Material;
import org.spout.api.plugin.CommonPlugin;
import org.spout.vanilla.event.block.SignChangeEvent;
import org.spout.vanilla.material.item.misc.Sign;
import org.teamcascade.spoutjiraplugin.config.Configuration;
import org.teamcascade.spoutjiraplugin.helpers.Either;
import org.teamcascade.spoutjiraplugin.helpers.Pair;
import org.teamcascade.spoutjiraplugin.helpers.task.Callback;
import org.teamcascade.spoutjiraplugin.helpers.task.Task;
import org.teamcascade.spoutjiraplugin.helpers.task.TaskExecutor;
import org.teamcascade.spoutjiraplugin.jira.client.JiraClient;
import org.teamcascade.spoutjiraplugin.jira.client.JiraError;
import org.teamcascade.spoutjiraplugin.jira.client.JiraIssue;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BlockListener implements Listener
{
    private static final String JIRA_SIGN_KEY = "{jira}";
    private static final String JIRA_ISSUE_KEY_REGEX = "\\{[A-Z]+-[0-9]+}"; // TODO: Ensure this is accurate.

    private final CommonPlugin parentPlugin;
    private final JiraClient jiraClient;
    private final TaskExecutor taskExecutor;
    private final Configuration config;
    private final Logger log;

    public BlockListener(final CommonPlugin parentPlugin, final JiraClient jiraClient, final TaskExecutor taskExecutor, final Logger log, final Configuration config)
    {
        this.parentPlugin = parentPlugin;
        this.jiraClient = jiraClient;
        this.log = log;
        this.taskExecutor = taskExecutor;
        this.config = config;
    }

    /**
     * When the text of a sign is changed, create or update a corresponding JIRA issue.
     */
    @EventHandler
    public void onSignChange(final SignChangeEvent event)
    {
        if (!isNewJiraSign(event))
            return;

        // Create a JIRA issue from this sign; strip away the prefix to get the issue text.
        final String issueSummary = getJiraIssueSummary(event);
        final Sign signBlock = event.getSign();
        final Point l = event.
        final World w = signBlock.getWorld();
        final String user = event.getPlayer().getDisplayName();

        taskExecutor.executeAsyncTask(new Task<Either<JiraIssue, JiraError>>()
                {
                    @Override
                    public Either<JiraIssue, JiraError> execute()
                    {
                        return jiraClient.createIssue(user, issueSummary, l.getWorld().getName(), l.getBlockX(), l.getBlockY(), l.getBlockZ());
                    }
                }, new Callback<Either<JiraIssue, JiraError>>()
        {
            @Override
            public void execute(Either<JiraIssue, JiraError> input)
            {
                if (input.getFirst() != null)
                {
                    parentPlugin.getEngine().getPlayer(user, true).sendMessage(("Created new JIRA Issue " + input.getFirst().getKey()));

                    Block blockLatest = w.getBlockAt(l);
                    log.info("The block in world " + w.getName() + " at position " + l.toString() + " is " + blockLatest.getType().toString());
                    if (blockLatest.getType().equals(Material.SIGN_POST) || blockLatest.getType().equals(Material.WALL_SIGN))
                    {
                        log.info("Preparing to update sign.");
                        Sign signage = (Sign) blockLatest.getState();
                        String lineOrig = signage.getLine(0);
                        lineOrig = lineOrig.replace("{jira}", "{" + input.getFirst().getKey() + "}");
                        log.info("New first line text: " + lineOrig);
                        signage.setLine(0, lineOrig);
                        signage.update();
                        log.info("Sign Updated.");
                    }
                }
                else
                {
                    parentPlugin.getEngine().getPlayer(user,true).sendMessage("Could not create new JIRA Issue :(");
                    String[] errorMessages = new String[input.getSecond().getErrorMessages().size()];
                    input.getSecond().getErrorMessages().toArray(errorMessages);
                    parentPlugin.getEngine().getPlayer(user,true).sendMessage(errorMessages);
                }
            }
        }
        );
    }

    /**
     * When a sign is destroyed, resolve the corresponding JIRA issue, if it exists.
     */
    @EventHandler
    public void onBlockBreak(final BlockBreakEvent event)
    {
        Block brokenBlock = event.getBlock();
        if (!brokenBlock.getType().equals(Material.SIGN_POST) && !brokenBlock.getType().equals(Material.WALL_SIGN))
        {
            if (config.isDebugLoggingEnabled())
            {
                log.info("Block was not a sign post, exiting.");
            }
            return;
        }

        if (!(brokenBlock.getState() instanceof Sign))
        {
            if (config.isDebugLoggingEnabled())
            {
                log.info("Block did not contain sign state, exiting.");
            }
            return;
        }

        Sign signage = (Sign) brokenBlock.getState();
        final Pair<Boolean, String> matchData = isExistingJiraSign(signage);
        if (!matchData.getLeft())
        {
            if (config.isDebugLoggingEnabled())
            {
                log.info("Sign was not a JIRA issue.");
            }
            return;
        }
        // Existing JIRA sign.
        final String issueKey = matchData.getRight();
        final String user = event.getPlayer().getDisplayName();
        log.info(String.format("Sign for issueKey %s was destroyed; issue should be resolved.", issueKey));

        final int x = brokenBlock.getX();
        final int y = brokenBlock.getY();
        final int z = brokenBlock.getZ();
        final String[] originalSignData = signage.getLines();

        // do it.
        taskExecutor.executeAsyncTask(new Task<Boolean>()
        {
            @Override
            public Boolean execute()
            {
                return jiraClient.resolveIssue(issueKey, user);
            }
        }, new Callback<Boolean>()
        {
            @Override
            public void execute(Boolean input)
            {
                if (!input)
                {
                    log.warning("Attempt to resolve JIRA Issue " + issueKey + " did not succeed, but the sign is being destroyed anyway.");
                    return;
                }

                // All good.
                parentPlugin.getEngine().getPlayer(user,true).sendMessage("Resolved JIRA issue " + issueKey);
            }
        }
        );
    }

    private boolean isNewJiraSign(SignChangeEvent event)
    {
        String firstLine = event.getLine(0);
        return firstLine.equalsIgnoreCase(JIRA_SIGN_KEY);
    }

    private Pair<Boolean, String> isExistingJiraSign(Sign sign)
    {
        Pattern issueKeyPattern = Pattern.compile(JIRA_ISSUE_KEY_REGEX);
        Matcher issueKeyMatcher = issueKeyPattern.matcher(sign.getLine(0));

        return new Pair<Boolean, String>(issueKeyMatcher.matches(),
                issueKeyMatcher.matches() ?
                        issueKeyMatcher.group().replace("{", "").replace("}", "") :
                        "");
    }

    private String getJiraIssueSummary(SignChangeEvent event)
    {
        final StringBuilder builder = new StringBuilder();
        for (String s : event.getText()){
            builder.append(s);
        }
        return builder.toString();
    }
}

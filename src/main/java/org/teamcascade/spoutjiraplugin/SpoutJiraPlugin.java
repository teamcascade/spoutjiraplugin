package org.teamcascade.spoutjiraplugin;


import org.spout.api.Engine;
import org.spout.api.Spout;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.plugin.PluginManager;
import org.teamcascade.spoutjiraplugin.commands.GoToIssueCommandExecutor;
import org.teamcascade.spoutjiraplugin.commands.JiraIssuesCommandExecutor;
import org.teamcascade.spoutjiraplugin.config.Configuration;
import org.teamcascade.spoutjiraplugin.config.ConfigurationLoader;
import org.teamcascade.spoutjiraplugin.helpers.task.TaskExecutor;
import org.teamcascade.spoutjiraplugin.jira.client.*;
import org.teamcascade.spoutjiraplugin.jira.client.auth.AuthenticatedResourceFactory;
import org.teamcascade.spoutjiraplugin.jira.client.auth.BasicAuthenticatedResourceFactory;
import org.teamcascade.spoutjiraplugin.listeners.BlockListener;
import org.teamcascade.spoutjiraplugin.listeners.LoginListener;

import java.util.logging.Logger;

/**
 * Overall orchestrator and criminal mastermind of the Minecraft/JIRA plugin.
 *
 * @author Joe Clark
 */
public class SpoutJiraPlugin extends CommonPlugin
{
    private Logger log;

    private JiraClient jiraClient;
    private TaskExecutor taskExecutor;
   /* private BlockListener blockListener;
    private LoginListener loginListener;*/
    private Engine engine;

    public void onLoad(){
        engine = Spout.getEngine();
    }

    /**
     * Invoked when the plugin is disabled; perform tearDown/cleanup here.
     */
    public void onDisable()
    {
        log.info("Disabled message here, shown in console on startup");
        // TODO: graceful cleanup
    }

    /**
     * Invoked when the plugin is enabled and/or the server is started; perform initialisation here.
     */
    public void onEnable()
    {
        log = getLogger();
        log.info("Enabling Minecraft JIRA plugin - http://bitbucket.org/jaysee00/minecraftjiraplugin");

        // Load plugin configuration from config.yml
        final Configuration config = loadConfiguration();

        // Use the configuration to login to Jira.
        AuthenticatedResourceFactory resourceFactory = new BasicAuthenticatedResourceFactory(config, log);
        if (!resourceFactory.login())
        {
            log.severe("*********************************************************");
            log.severe("* Unable to login in to JIRA. Check your configuration. *");
            log.severe("*********************************************************");
        }

        // Load components
        jiraClient = new DefaultJiraClient(log, resourceFactory, config.getLocationCustomFieldId(), config.getMinecraftProjectKey(), config.getJiraAdminUsername());
        final ActivityStreamClient activityStreamClient = new DefaultActivityStreamClient(this, config, resourceFactory);
        final JiraUserClient jiraUserClient = new JiraUserClient(log, config);


        taskExecutor = new TaskExecutor(this, engine.getScheduler());

        engine.getEventManager().registerEvents(new BlockListener(this, jiraClient, taskExecutor, log, config), this);
        engine.getEventManager().registerEvents(new LoginListener(this, config, jiraUserClient, activityStreamClient, taskExecutor), this);


        // Register block event listeners - code that executes when the world environment is manipulated.
        final PluginManager pluginManager = engine.getPluginManager();


        // Register command executors - code that executes in response to player /slash commands, or commands via the server console.
        getCommand("jiraIssues").setExecutor(new JiraIssuesCommandExecutor(taskExecutor, log, jiraClient));
        getCommand("gotoIssue").setExecutor(new GoToIssueCommandExecutor(this, log, jiraClient));
    }

    private Configuration loadConfiguration()
    {
        getConfig().options().copyDefaults(true);
        saveConfig();

        final ConfigurationLoader loader = new ConfigurationLoader(getConfig(), log);
        return loader.load();
    }


}

package org.teamcascade.spoutjiraplugin.jira.client;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.simple.JSONObject;
import org.spout.api.entity.Player;
import org.spout.api.plugin.CommonPlugin;
import org.teamcascade.spoutjiraplugin.config.Configuration;
import org.teamcascade.spoutjiraplugin.jira.client.auth.AuthenticatedResourceFactory;
import org.teamcascade.spoutjiraplugin.jira.client.resource.ActivityBuilder;

import java.util.logging.Logger;

/**
 *
 */
public class DefaultActivityStreamClient implements  ActivityStreamClient
{
    private final AuthenticatedResourceFactory authenticatedResourceFactory;
    private final Logger log;
    private final Configuration pluginConfig;
    private long counter = 0;


    public DefaultActivityStreamClient(CommonPlugin plugin, Configuration pluginConfig, AuthenticatedResourceFactory authenticatedResourceFactory)
    {
        this.authenticatedResourceFactory = authenticatedResourceFactory;
        this.pluginConfig = pluginConfig;
        this.log = plugin.getLogger();
    }

    @Override
    public void postActivity(Player actor, String titleHtml, String contentHtml)
    {
        counter++;

        ActivityBuilder builder = ActivityBuilder.get();
        JSONObject activity = builder.setActor(pluginConfig.getJiraAdminUsername()) // TODO: Use the current user's name.
                                     .setTitle(titleHtml)
                                     .setContent(contentHtml)
                                     .setId("http://minecraft.net/" + String.valueOf(counter))
                                     .build();

        // POST to /rest/activities/1.0
        WebResource.Builder activityResource = authenticatedResourceFactory.getResource("/rest/activities/1.0/");
        activityResource.type("application/vnd.atl.streams.thirdparty+json");
        activityResource.entity(activity);

        ClientResponse response = activityResource.post(ClientResponse.class);
        log.info("Posted and returned: " + response.getClientResponseStatus().getReasonPhrase());
    }
}
